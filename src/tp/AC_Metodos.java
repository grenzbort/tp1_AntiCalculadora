package tp;

import java.util.ArrayList;
import java.util.Random;

public class AC_Metodos
{
	ArrayList <String> operadoresObjetivo= new ArrayList<>();
	ArrayList <String> ecuacionUsuario= new ArrayList<>();
	ArrayList <String> operadoresUsuario = new ArrayList<>();
	
	int dificultad, numeroObjetivo;
	float puntaje;
	
	Random rd = new Random();
	
	AC_Metodos()
	{
		dificultad = 1;
		setOperadores();
		generarObjetivo();
	}
	
	public void setDificultad(int dif)
	{
		dificultad = dif;
	}
	
	public void setOperadores()
	{	
		for (int i = 0; i < this.dificultad; i++)
		{
			operadoresObjetivo.add(elegirOperador());	
		}	
	}
	
	public String renovarOperadores()
	{
		operadoresObjetivo.clear();
		setOperadores();
		return mostrarOperadores();
	}
	
	public String renovarObjetivo()
	{
		generarObjetivo();
		return mostrarObjetivo();
	}
	
	private int generarOperadores()
	{
		int operadores = rd.nextInt(4);
		return operadores;	
	}
	
	private void generarObjetivo()
	{
		switch (dificultad)
		{
			case 1: 
				numeroObjetivo = rd.nextInt(101); 
				break;
			case 2:	
				numeroObjetivo = rd.nextInt(1001);
				break;
			case 3: 
				numeroObjetivo = rd.nextInt(10001);
				break;
		}
	}
	
	private String elegirOperador () 
	{
		String ret = "";
		switch (generarOperadores())
		{
			case 0: 
				ret = "+"; 
				break;
			case 1:	
				ret = "*";
				break;
			case 2: 
				ret = "-";
				break;
			case 3: 
				ret = "/";
				break;
		}
		return ret;
	}
	
	public String mostrarObjetivo()
	{
		return String.valueOf(numeroObjetivo);
	}
	
	public String mostrarOperadores()
	{
		StringBuilder sb = new StringBuilder();
		for (String s : operadoresObjetivo)
		{
		    sb.append(s);
		    sb.append("\t");
		}
		String obj;
		obj = sb.toString();
		 
		return obj;
	}
	
	private int stringToInt(String respuesta)
	{
		try
		{
			return Integer.parseInt(respuesta);
		}
		catch(Exception ex)
		{
			return 0;
		}
	}
	
	private boolean resolver()
	{
		boolean ver = false;
		
		while (this.ecuacionUsuario.size() > 1)
		{
			int indice = 0;
			boolean check = true;
			
			for (int i = 0; i < this.ecuacionUsuario.size(); i++)
			{
				
				if (this.ecuacionUsuario.get(i).equals("*")|| this.ecuacionUsuario.get(i).equals("/"))
				{
					indice = i;
					break;
				}
				if ((this.ecuacionUsuario.get(i).equals("+") || this.ecuacionUsuario.get(i).equals("-")) && check)
				{
					indice = i;
					check = false;
				}
			}
			
			operarResolver(indice);
			
		}
		
		if (stringToInt(this.ecuacionUsuario.get(0)) == numeroObjetivo)
		{
			ver = true;
		}
		return ver;
	}

	private void operarResolver(int indice) {

		int indice1 = stringToInt(this.ecuacionUsuario.get(indice-1));
		int indice2 = stringToInt(this.ecuacionUsuario.get(indice+1));
		
		if (this.ecuacionUsuario.get(indice).equals("*"))
		{
			indice1 = indice1 * indice2;
			preResultado(indice, indice1);
		}
		else if (this.ecuacionUsuario.get(indice).equals("/"))
		{
			indice1 = indice1 / indice2;
			preResultado(indice, indice1);
		}
		else if (this.ecuacionUsuario.get(indice).equals("+"))
		{
			indice1 = indice1 + indice2;
			preResultado(indice, indice1); 
		}
		else if (this.ecuacionUsuario.get(indice).equals("-"))
		{
			indice1 = indice1 - indice2;
			preResultado(indice, indice1);
		}
	}

	private void preResultado(int indice, int indice1)
	{
		this.ecuacionUsuario.set(indice-1, String.valueOf(indice1));
		this.ecuacionUsuario.remove(indice+1);
		this.ecuacionUsuario.remove(indice);
		this.ecuacionUsuario.trimToSize();
	}
	
	public boolean controlarOperadores()
	{
		operadoresUsuario.clear();
		for (int i = 0; i < ecuacionUsuario.size(); i++)
		{
			if (this.ecuacionUsuario.get(i).equals("+") || this.ecuacionUsuario.get(i).equals("-") ||
				this.ecuacionUsuario.get(i).equals("*") || this.ecuacionUsuario.get(i).equals("/"))
			{
				operadoresUsuario.add(this.ecuacionUsuario.get(i));
			}
		}
		
		if (this.operadoresObjetivo.size() == operadoresUsuario.size() && this.operadoresObjetivo.containsAll(operadoresUsuario))
		{
			return true;
		}
		return false;		
	}

	public boolean controlarResultado()
	{
		if(controlarOperadores() && resolver())
		{
			this.ecuacionUsuario.clear();
			puntaje = puntaje + (10*dificultad);
			return true;
		}
		this.ecuacionUsuario.clear();
		puntaje = puntaje/2;
		return false;
	}

	public void aņadirElemento(String nuevoElemento)
	{
		ecuacionUsuario.add(nuevoElemento);
	}

	public String actualizarPuntaje()
	{
		return String.valueOf(puntaje);
	}
	
	public void eliminarCaracter()
	{
		ecuacionUsuario.remove(ecuacionUsuario.size()-1);
		
	}
	
	public boolean chequearIngreso()
	{
		if(this.ecuacionUsuario.get(0).equals("+") || this.ecuacionUsuario.get(0).equals("-") || this.ecuacionUsuario.get(0).equals("*") || 
				this.ecuacionUsuario.get(0).equals("/") || this.ecuacionUsuario.get(this.ecuacionUsuario.size()-1).equals(""))
		{
			this.ecuacionUsuario.clear();
			return true;
		}
		return false;
	}

	public void vaciarIngreso()
	{
		this.ecuacionUsuario.clear();
	}
	
}