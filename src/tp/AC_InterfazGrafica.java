package tp;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextPane;

public class AC_InterfazGrafica {
	
	private JFrame frmAnticalculadora;
	
	String ingreso = "";
	private JTextField txt_usuario;
	private final ButtonGroup botonesDificultad = new ButtonGroup();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AC_InterfazGrafica window = new AC_InterfazGrafica();
					window.frmAnticalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AC_InterfazGrafica() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		AC_Metodos Juego = new AC_Metodos();
		
		frmAnticalculadora = new JFrame();
		frmAnticalculadora.setResizable(false);
		frmAnticalculadora.setTitle("Anti-calculadora");
		frmAnticalculadora.getContentPane().setBackground(new Color(70, 130, 180));
		frmAnticalculadora.setBounds(100, 100, 653, 500);
		frmAnticalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAnticalculadora.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel.setBounds(10, 190, 340, 227);
		frmAnticalculadora.getContentPane().add(panel);
		panel.setLayout(null);

		/////////////////////////////////CODIGO JLABELS/////////////////////////////////

		JLabel lblResultado = new JLabel("");
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblResultado.setBounds(10, 152, 320, 66);
		panel.add(lblResultado);

		JLabel lblOperadores = new JLabel(Juego.mostrarOperadores());
		lblOperadores.setForeground(Color.WHITE);
		lblOperadores.setHorizontalAlignment(SwingConstants.CENTER);
		lblOperadores.setFont(new Font("Agency FB", Font.PLAIN, 30));
		lblOperadores.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		lblOperadores.setOpaque(true);
		lblOperadores.setBackground(new Color(128, 128, 128));
		lblOperadores.setBounds(10, 78, 230, 60);
		frmAnticalculadora.getContentPane().add(lblOperadores);		
		
		JLabel lblObjetivo = new JLabel(Juego.mostrarObjetivo());
		lblObjetivo.setForeground(Color.WHITE);
		lblObjetivo.setHorizontalAlignment(SwingConstants.CENTER);
		lblObjetivo.setFont(new Font("Agency FB", Font.PLAIN, 30));
		lblObjetivo.setOpaque(true);
		lblObjetivo.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		lblObjetivo.setBackground(new Color(128, 128, 128));
		lblObjetivo.setBounds(245, 78, 103, 60);
		frmAnticalculadora.getContentPane().add(lblObjetivo);

		JLabel lblPuntaje = new JLabel("Puntaje:");
		lblPuntaje.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPuntaje.setBounds(10, 432, 102, 29);
		frmAnticalculadora.getContentPane().add(lblPuntaje);
		
		JLabel lblPuntos = new JLabel("0");
		lblPuntos.setHorizontalAlignment(SwingConstants.LEFT);
		lblPuntos.setFont(new Font("Agency FB", Font.BOLD, 20));
		lblPuntos.setBounds(110, 432, 240, 29);
		frmAnticalculadora.getContentPane().add(lblPuntos);

		JLabel lblSeleccionDificultad = new JLabel("Seleccione la cantidad de operadores a utilizar");
		lblSeleccionDificultad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSeleccionDificultad.setBounds(40, 10, 285, 23);
		frmAnticalculadora.getContentPane().add(lblSeleccionDificultad);

		/////////////////////////////////FIN CODIGO JLABELS/////////////////////////////////

		/////////////////////////////////CODIGO JTEXTFIELD/////////////////////////////////

		txt_usuario = new JTextField();
		txt_usuario.setHorizontalAlignment(SwingConstants.CENTER);
		txt_usuario.setFont(new Font("Agency FB", Font.BOLD, 17));
		txt_usuario.setBounds(10, 148, 340, 30);
		frmAnticalculadora.getContentPane().add(txt_usuario);
		txt_usuario.setColumns(10);
		
		JTextPane txt_Reglas = new JTextPane();
		txt_Reglas.setEditable(false);
		txt_Reglas.setForeground(Color.WHITE);
		txt_Reglas.setBackground(Color.GRAY);
		txt_Reglas.setFont(new Font("Tahoma", Font.BOLD, 11));
		txt_Reglas.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		txt_Reglas.setText("\r\n\tANTI-CALCULADORA\r\n\r\nObjetivo: tienes que llegar al n\u00FAmero que aparece en el cuadro de la derecha utilizando los operadores que se encuentran en el cuadro de la izquierda. Puedes utilizarlos en el orden que quieras pero tienes que usar cada uno de ellos y no otros.\r\n\r\nEJEMPLO:\r\nOperadores: + -\tObjetivo: 15\r\nCorrecto: \" 10+10-5 \"    \" 20-15+10 \"\r\nIncorrecto: \" 5*4-5 \"    \" 5+5+5 \"\r\n\r\n       \t    DIFICULTAD\r\nNivel 1: 1 operador, objetivo de 0 a 100, 10 puntos por operaci\u00F3n correcta.\r\n\r\nNivel 2: 2 operador, objetivo de 0 a 1000, 20 puntos por operaci\u00F3n correcta.\r\n\r\nNivel 3: 3 operador, objetivo de 0 a 10000, 30 puntos por operaci\u00F3n correcta.\r\n\r\nEn cualquier dificultad, cuando la respuesta sea incorrecta, se restar\u00E1n la mitad de los puntos actuales.");
		txt_Reglas.setBounds(358, 10, 279, 407);
		frmAnticalculadora.getContentPane().add(txt_Reglas);

		/////////////////////////////////FIN CODIGO JTEXTFIELD/////////////////////////////////

		/////////////////////////////////CODIGO JBUTTONS Y JRADIOBUTTONS/////////////////////////////////

		JRadioButton radioButton1 = new JRadioButton("1");
		radioButton1.setFont(new Font("Agency FB", Font.BOLD, 19));
		radioButton1.setSelected(true);
		radioButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ingreso = "";
				txt_usuario.setText(null);
				Juego.setDificultad(1);
				lblOperadores.setText(Juego.renovarOperadores());
				lblObjetivo.setText(Juego.renovarObjetivo());
			}
		});
		radioButton1.setBackground(new Color(70, 130, 180));
		botonesDificultad.add(radioButton1);
		radioButton1.setBounds(110, 35, 36, 23);
		frmAnticalculadora.getContentPane().add(radioButton1);
		
		JRadioButton radioButton2 = new JRadioButton("2");
		radioButton2.setFont(new Font("Agency FB", Font.BOLD, 19));
		radioButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingreso = "";
				txt_usuario.setText(null);
				Juego.setDificultad(2);
				lblOperadores.setText(Juego.renovarOperadores());
				lblObjetivo.setText(Juego.renovarObjetivo());
			}
		});
		radioButton2.setBackground(new Color(70, 130, 180));
		botonesDificultad.add(radioButton2);
		radioButton2.setBounds(155, 35, 36, 23);
		frmAnticalculadora.getContentPane().add(radioButton2);
		
		JRadioButton radioButton3 = new JRadioButton("3");
		radioButton3.setFont(new Font("Agency FB", Font.BOLD, 19));
		radioButton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingreso = "";
				txt_usuario.setText(null);
				Juego.setDificultad(3);
				lblOperadores.setText(Juego.renovarOperadores());
				lblObjetivo.setText(Juego.renovarObjetivo());
			}
		});
		radioButton3.setBackground(new Color(70, 130, 180));
		botonesDificultad.add(radioButton3);
		radioButton3.setBounds(200, 35, 36, 23);
		frmAnticalculadora.getContentPane().add(radioButton3);
		

		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(118, 115, 103, 30);
		panel.add(btnReset);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(null);
			}
		});
		btnReset.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_Division = new JButton("/");
		btn_Division.setBounds(205, 80, 60, 30);
		panel.add(btn_Division);
		btn_Division.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_Division.getText());
				if(!ingreso.equals(""))
				{
					Juego.añadirElemento(ingreso);
				}
				Juego.añadirElemento("/");
				ingreso = "";
			}
		});
		btn_Division.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_Multiplicacion = new JButton("*");
		btn_Multiplicacion.setBounds(140, 80, 60, 30);
		panel.add(btn_Multiplicacion);
		btn_Multiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_Multiplicacion.getText());
				if(!ingreso.equals(""))
				{
					Juego.añadirElemento(ingreso);
				}
				Juego.añadirElemento("*");
				ingreso = "";
			}
		});
		btn_Multiplicacion.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_Resta = new JButton("-");
		btn_Resta.setBounds(75, 80, 60, 30);
		panel.add(btn_Resta);
		btn_Resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_Resta.getText());
				if(!ingreso.equals(""))
				{
					Juego.añadirElemento(ingreso);
				}
				Juego.añadirElemento("-");
				ingreso = "";
			}
		});
		btn_Resta.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_Suma = new JButton("+");
		btn_Suma.setBounds(10, 80, 60, 30);
		panel.add(btn_Suma);
		btn_Suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_Suma.getText());
				if(!ingreso.equals(""))
				{
					Juego.añadirElemento(ingreso);
				}
				Juego.añadirElemento("+");
				ingreso = "";
			}
		});
		btn_Suma.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_9 = new JButton("9");
		btn_9.setBounds(270, 45, 60, 30);
		panel.add(btn_9);
		btn_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_9.getText());
				ingreso += "9";
			}
		});
		btn_9.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_8 = new JButton("8");
		btn_8.setBounds(205, 45, 60, 30);
		panel.add(btn_8);
		btn_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_8.getText());
				ingreso += "8";
			}
		});
		btn_8.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_7 = new JButton("7");
		btn_7.setBounds(140, 45, 60, 30);
		panel.add(btn_7);
		btn_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_7.getText());
				ingreso += "7";
			}
		});
		btn_7.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_6 = new JButton("6");
		btn_6.setBounds(75, 45, 60, 30);
		panel.add(btn_6);
		btn_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_6.getText());
				ingreso += "6";
			}
		});
		btn_6.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_5 = new JButton("5");
		btn_5.setBounds(10, 45, 60, 30);
		panel.add(btn_5);
		btn_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_5.getText());
				ingreso += "5";
			}
		});
		btn_5.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_4 = new JButton("4");
		btn_4.setBounds(270, 10, 60, 30);
		panel.add(btn_4);
		btn_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_4.getText());
				ingreso += "4";
			}
		});
		btn_4.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_3 = new JButton("3");
		btn_3.setBounds(205, 10, 60, 30);
		panel.add(btn_3);
		btn_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_3.getText());
				ingreso += "3";
			}
		});
		btn_3.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_2 = new JButton("2");
		btn_2.setBounds(140, 10, 60, 30);
		panel.add(btn_2);
		btn_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_2.getText());
				ingreso += "2";
			}
		});
		btn_2.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_1 = new JButton("1");
		btn_1.setBounds(75, 10, 60, 30);
		panel.add(btn_1);
		btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_1.getText());
				ingreso += "1";
			}
		});
		btn_1.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_0 = new JButton("0");
		btn_0.setBounds(10, 10, 60, 30);
		panel.add(btn_0);
		btn_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_usuario.setText(txt_usuario.getText() + btn_0.getText());
				ingreso += "0";
			}
		});
		btn_0.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ingreso = "";
				txt_usuario.setText(null);
				Juego.vaciarIngreso();
				lblOperadores.setText(Juego.renovarOperadores());
				lblObjetivo.setText(Juego.renovarObjetivo());
			}
		});
		btnNuevo.setBounds(226, 115, 103, 30);
		panel.add(btnNuevo);
		btnNuevo.setFont(new Font("Agency FB", Font.BOLD, 16));
		
		JButton btn_Igual = new JButton("=");
		btn_Igual.setBounds(270, 80, 60, 30);
		panel.add(btn_Igual);
		btn_Igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				try 
				{
					Juego.añadirElemento(ingreso); 
					
					if (Juego.chequearIngreso()) {
						lblResultado.setText("¡Ingreso Incorrecto!");
						lblResultado.setForeground(Color.yellow);
					}
					else if (Juego.controlarResultado()) 
					{
						lblResultado.setText("¡CORRECTO!");
						lblResultado.setForeground(Color.GREEN);
					}
					else 
					{
						lblResultado.setText("¡INCORRECTO!");
						lblResultado.setForeground(Color.RED);
					}
					ingreso = "";
					txt_usuario.setText(null);
					lblOperadores.setText(Juego.renovarOperadores());
					lblObjetivo.setText(Juego.renovarObjetivo());
					lblPuntos.setText(Juego.actualizarPuntaje());
				}
				catch(Exception ex)
				{
					lblResultado.setText("¡Ingreso Incorrecto!");
					lblResultado.setForeground(Color.yellow);
					ingreso = "";
					Juego.vaciarIngreso();
					txt_usuario.setText(null);
				}
			}
		});
		btn_Igual.setFont(new Font("Adonais", Font.PLAIN, 16));	
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
				if(ingreso.equals("")) {
					Juego.eliminarCaracter();
					txt_usuario.setText(txt_usuario.getText().substring(0, txt_usuario.getText().length()-1));
				}
				else {
					ingreso = ingreso.substring(0, ingreso.length() - 1);
					txt_usuario.setText(txt_usuario.getText().substring(0, txt_usuario.getText().length()-1));
				}
				}
				catch(Exception ex)
				{
					lblResultado.setText("¡Nada que borrar!");
					lblResultado.setForeground(Color.yellow);
				}
			}
		});
		btnBorrar.setFont(new Font("Agency FB", Font.BOLD, 16));
		btnBorrar.setBounds(10, 115, 103, 30);
		panel.add(btnBorrar);
	
		/////////////////////////////////FIN CODIGO JBUTTONS Y JRADIOBUTTONS/////////////////////////////////
	}
}